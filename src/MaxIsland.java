import java.util.ArrayList;
import java.util.List;

public class MaxIsland {

	public static void main(String[] args) {
		String input = args[0];
		int dim = Integer.parseInt(args[1]);

		new MaxIsland().solve(input, dim);
	}

	List<Island> islands = new ArrayList<Island>();
	Island currIsland;

	public void solve(String input, int dim) {
		parseInput(input, dim);

		Island maxIsland = null;
		int maxSum = 0;
		for (Island island : islands) {
			int sum = sumIsland(island);

			if (sum > maxSum) {
				maxSum = sum;
				maxIsland = island;
			}
		}
		
		System.out.println(maxIsland);
	}

	private int sumIsland(Island island) {
		return island.getChildren().stream()
				.reduce(0, (acc, x) -> acc + x.getWeight(), (x, y) -> x + y);
	}

	private List<Block> parseInput(String input, int dim) {
		List<Block> blocks = parseBlocks(input, dim);

		for (Block block : blocks) {
			if (block.isLand()) {
				if (block.getIsland() == null) {
					currIsland = new Island();
					block.setIsland(currIsland);
					islands.add(currIsland);
				}

				Block toSouth = getSouthBlock(blocks, block, dim);
				Block toEast = getEastBlock(blocks, block, dim);

				if (toSouth != null && toSouth.isLand()) {
					toSouth.setIsland(block.getIsland());
				}

				if (toEast != null && toEast.isLand() && toEast.getIsland() == null) {
					toEast.setIsland(block.getIsland());
				}
			}
		}

		return blocks;
	}

	private List<Block> parseBlocks(String input, int dim) {
		List<Block> blocks = new ArrayList<Block>();

		for (int row = 0; row < dim; row++) {
			for (int col = 0; col < dim; col++) {
				int weight = getWeight(input, row, col, dim);

				blocks.add(new Block(weight, row, col));
			}
		}

		return blocks;
	}

	private Block getEastBlock(List<Block> blocks, Block block, int dim) {
		if (block.getCol() == dim - 1) {
			return null;
		}

		return blocks.get((block.getRow() * dim) + block.getCol() + 1);
	}

	private Block getSouthBlock(List<Block> blocks, Block block, int dim) {
		if (block.getRow() == dim - 1) {
			return null;
		}

		return blocks.get((block.getRow() + 1) * dim  + block.getCol());
	}

	private int getWeight(String input, int row, int col, int dim) {
		return Character.getNumericValue(input.charAt((row * dim) + col));
	}

}