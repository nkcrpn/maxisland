import java.util.ArrayList;
import java.util.List;

public class Island {

	private static int currNum = 0;

	private String name;
	private List<Block> children = new ArrayList<Block>();

	public Island() {
		name = "island" + currNum;
		currNum++;
	}

	public List<Block> getChildren() {
		return children;
	}

	public void addChild(Block block) {
		children.add(block);
	}

	@Override
	public String toString() {
		String childString = children
				.stream()
				.collect(StringBuilder::new, (acc, x) -> acc.append(" " + x.getWeight()),
						StringBuilder::append).toString();
		return name + childString;
	}
}
