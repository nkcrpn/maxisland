public class Block {

	// Top lel
	private boolean isLand;
	private Island island;
	
	private int weight;
	

	private int col;
	private int row;

	public Block() {
	}

	public Block(int weight, int row, int col) {
		this.weight = weight;
		this.row = row;
		this.col = col;

		this.isLand = (weight != 0);
	}

	public boolean isLand() {
		return isLand;
	}

	public void setLand(boolean isLand) {
		this.isLand = isLand;
	}

	public Island getIsland() {
		return island;
	}

	public void setIsland(Island island) {
		this.island = island;
		
		island.addChild(this);
	}

	public int getWeight() {
		return weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;

		isLand = (weight != 0);
	}

	public int getCol() {
		return col;
	}

	public void setCol(int col) {
		this.col = col;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

}
